<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package galileen
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <!-- Google fonts-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />

  <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="page-top">
    
<?php wp_body_open();?>

<header class="header">
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a href="<?php echo home_url( '/' ); ?>" class="navbar-brand js-scroll-trigger">
        <img src="<?php echo get_template_directory_uri(); ?>/images/navbar-logo.png" class="logo img-fluid">
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ml-1"></i>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <?php wp_nav_menu(
          array(
            'theme_location'  => 'header', 
            'container' => '', 
            'menu_class' => 'navbar-nav text-uppercase ml-auto',
          )
        ); 
      ?>
      </div>
    </div>
  </nav>
</header>