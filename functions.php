<?php
/**
 * galileen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package galileen
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'galileen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function galileen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on galileen, use a find and replace
		 * to change 'galileen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'galileen', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/* This theme uses wp_nav_menu() in one location.
        *  header and footer menu
        */
		register_nav_menus( array(
            'header' => 'haut de page',
            'footer' => 'Bas de page',
            'header-menu' => 'second haut de page'
        ) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'galileen_setup' );

/**
 * Enqueue scripts and styles.
 */
function galileen_register_assets() {

	// Déclarer style.css à la racine du thème
	wp_enqueue_style( 
		'galileen-style',
		get_stylesheet_uri(),
		array(), 
		'1.0.0' 
	);
	
	// Déclarer bootstrap css
	wp_enqueue_style( 
        'galileen_bootstrap_css', 
        get_template_directory_uri() . '/bootstrap/bootstrap.min.css',
        array(), 
        '1.0.0'
	);

	// Déclarer fontawesome css
	wp_enqueue_style( 
        'galileen_fontawesome_css', 
        get_template_directory_uri() . '/fontawesome/all.min.css',
        array(), 
        '1.0.0'
	);
	
	// Jquery
	wp_enqueue_script('galileen_jquery', 
		'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', 
		array('jquery'), 
		true
    );

	 // Déclarer le JS
	 wp_enqueue_script( 
        'galileen_script_js', 
        get_template_directory_uri() . '/js/script.js', 
        array('jquery'), 
        '1.0.0', 
        true
	);
	
	// Déclarer bootstrap js
	wp_enqueue_script( 
        'galileen_popper_js', 
        get_template_directory_uri() . '/bootstrap/popper.min.js', 
        array('jquery'),
        '1.0.0', 
        true
	);

	wp_enqueue_script( 
        'galileen_bootstrap_js', 
        get_template_directory_uri() . '/bootstrap/bootstrap.min.js', 
        array('jquery'),
        '1.0.0', 
        true
	);
	
	
	
	// Déclarer fontawesome js
	wp_enqueue_script( 
        'galileen_fontawesome_js', 
        get_template_directory_uri() . '/fontawesome/all.min.js', 
        array('jquery'),
        '1.0.0', 
        true
	);
}
add_action( 'wp_enqueue_scripts', 'galileen_register_assets' );

function galileen__register_post_types() {
	// CPT portfolio
	register_post_type( 'portfolio',
	array(
		'labels' => array (
			'name' => __( 'Portfolio', 'galileen' ),
			'singular_name' => __( 'portfolio', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'portfolio', 'with_front' => true ),
		'menu_icon' => 'dashicons-portfolio',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
	// CPT vestes
	register_post_type( 'veste',
	array(
		'labels' => array (
			'name' => __( 'Vestes', 'galileen' ),
			'singular_name' => __( 'veste', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'veste', 'with_front' => true ),
		'menu_icon' => 'dashicons-camera',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
	// CPT vesteGalileen
	register_post_type( 'vesteGalileen',
	array(
		'labels' => array (
			'name' => __( 'VestesGalileen', 'galileen' ),
			'singular_name' => __( 'vesteGalileen', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'vesteGalileen', 'with_front' => true ),
		'menu_icon' => 'dashicons-camera-alt',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
	// CPT accessoire
	register_post_type( 'accessoire',
	array(
		'labels' => array (
			'name' => __( 'Accessoires', 'galileen' ),
			'singular_name' => __( 'accessoire', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'accessoire', 'with_front' => true ),
		'menu_icon' => 'dashicons-image-filter',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
	// CPT soulier
	register_post_type( 'soulier',
	array(
		'labels' => array (
			'name' => __( 'Souliers', 'galileen' ),
			'singular_name' => __( 'soulier', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'soulier', 'with_front' => true ),
		'menu_icon' => 'dashicons-images-alt',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
	// CPT mariage
	register_post_type( 'mariage',
	array(
		'labels' => array (
			'name' => __( 'Mariage', 'galileen' ),
			'singular_name' => __( 'mariage', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'mariage', 'with_front' => true ),
		'menu_icon' => 'dashicons-cart',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
	// CPT designInterieur
	register_post_type( 'designInterieur',
	array(
		'labels' => array (
			'name' => __( 'designInterieur', 'galileen' ),
			'singular_name' => __( 'designInterieur', 'galileen' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'has_archive' => true,
		'rewrite' => array( 'slug' => 'designInterieur', 'with_front' => true ),
		'menu_icon' => 'dashicons-editor-unlink',
		'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' ),
		'exclude_from_search' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',
		)
	);
}
add_action( 'init', 'galileen__register_post_types' );

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

