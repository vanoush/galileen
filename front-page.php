<?php
/**
 * The template for displaying all pages
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package galileen
 */

get_header();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<main id="primary" class="site-main">

        <?php 
			$home_page = get_field('home_page');
			$header_section_one = $home_page['header_section_one'];
			$header_section_two = $home_page['header_section_two'];
			$header_section_button = $home_page['header_section_button'];
			$about_title = $home_page['about_title'];
			$about_sub_title = $home_page['about_sub_title'];
			$about_title_one = $home_page['about_title_one'];
			$about_sub_title_one = $home_page['about_sub_title_one'];
			$about_title_two = $home_page['about_title_two'];
			$about_sub_title_two = $home_page['about_sub_title_two'];
			$about_title_three = $home_page['about_title_three'];
			$about_sub_title_three = $home_page['about_sub_title_three'];
			$about_title_four = $home_page['about_title_four'];
			$about_sub_title_four = $home_page['about_sub_title_four'];
            $about_title_five = $home_page['about_title_five'];
            $about_sub_title_five = $home_page['about_sub_title_five'];
            $about_message = $home_page['about_message'];
            $about_one_image = $home_page['about_one_image'];
            $about_two_image = $home_page['about_two_image'];
            $about_three_image = $home_page['about_three_image'];
            $about_four_image = $home_page['about_four_image'];
            $about_five_image = $home_page['about_five_image'];
            $portfolio_title = $home_page['portfolio_title'];
            $portfolio_sub_title = $home_page['portfolio_sub_title'];
            $portfolio_button = $home_page['portfolio_button'];
            $contact_title = $home_page['contact_title'];
            $contact_sub_title = $home_page['contact_sub_title'];
            $contact_button = $home_page['contact_button'];
		?>
		<!-- Masthead-->
		<header class="masthead">
				<div class="container">
					<div class="masthead-subheading"><?php echo $header_section_one ; ?></div>
					<div class="masthead-heading"><?php echo $header_section_two ; ?></div>
					<a class="btn btn-outline-primary btn-xl js-scroll-trigger" href="<?php echo home_url( '#about' ); ?>"><?php echo $header_section_button ; ?></a>
				</div>
		</header>

		<!-- About-->
        <section class="page-section" id="about">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase"><?php echo $about_title ; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $about_sub_title ; ?></h3>
                </div>
                <ul class="timeline">
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about/1.jpg" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading"><?php echo $about_title_one ; ?></h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted"><?php echo $about_sub_title_one ; ?></p></div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about/2.jpg" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading"><?php echo $about_title_two ; ?></h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted"><?php echo $about_sub_title_one ; ?></p></div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about/c3.jpeg" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading"><?php echo $about_title_three ; ?></h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted"><?php echo $about_sub_title_three ; ?></p></div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about/4.jpg" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading"><?php echo $about_title_four ; ?></h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted"><?php echo $about_sub_title_four ; ?></p></div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about/5.jpg" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading"><?php echo $about_title_five ; ?></h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted"><?php echo $about_sub_title_five ; ?></p></div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>
                                <?php echo $about_message ; ?>
                            </h4>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase"><?php echo $portfolio_title ; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $portfolio_sub_title ; ?></h3>
                </div>
				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'portfolio',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$portfolio_query = new WP_Query( $args );

					if( $portfolio_query -> have_posts() ) : while( $portfolio_query -> have_posts() ) : $portfolio_query -> the_post(); ?>
				
					<div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'portfolio-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>
                <div class="container my-5 text-center">
                  <a class="btn btn-outline-primary btn-xl js-scroll-trigger" href="<?php echo home_url( 'gallery' ); ?>"><?php echo $portfolio_button ; ?></a>
                </div>
            </div>
        </section>

		<!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase"><?php echo $contact_title ; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $contact_sub_title ; ?></h3>
                </div>
                <form id="contactForm" name="sentMessage" novalidate="novalidate">
                    <div class="text-center">
                        <a class="btn btn-outline-primary btn-xl" href="<?php echo home_url( 'contact' ); ?>"><?php echo $contact_button ; ?></a>
                    </div>
                </form>
            </div>
        </section>
		

	</main><!-- #main -->

<?php
get_footer();
