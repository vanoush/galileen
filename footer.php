<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package galileen
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<!-- Footer-->
			<footer class="footer py-4">
				<div class="container">
					<div class="row align-items-center justify-content-start">
						<div class="col-lg-4 text-lg-left">Copyright © Galiléen</div>
						<!-- <div class="col-lg-4 my-3 my-lg-0">
							<a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a>
							<a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
							<a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
						</div>
						<div class="col-lg-4 text-lg-right">
							<a class="mr-3" href="#!">Privacy Policy</a>
							<a href="#!">Terms of Use</a>
						</div> -->
					</div>
				</div>
            </footer>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
