<?php
/**
 * Template name: Contact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package galileen
 */

get_header('menu');
?>

	<main id="primary" class="site-main">
    <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contactez-nous</h2>
                    <h3 class="section-subheading text-muted">Envoyez nous un mail pour notifier vos besoins.</h3>
                </div>
                <div class="">
                    <?php echo do_shortcode('[gravityform id=1 name=ContactUs title=false description=false]'); ?>
                </div>
            </div>
        </section>

	</main><!-- #main -->

<?php
get_footer();