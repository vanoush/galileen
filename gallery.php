<?php
/**
 * Template name: Gallery
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package galileen
 */

get_header('menu');
?>

	<main id="primary" class="site-main">

		<?php 
			$gallery_page = get_field('gallery_page');
			$gallery_header_title = $gallery_page['gallery_header_title'];
			$gallery_header_sub_title = $gallery_page['gallery_header_sub_title'];
			$gallery_header_button = $gallery_page['gallery_header_button'];
			$gallery_portfolio_title = $gallery_page['gallery_portfolio_title'];
			$gallery_portfolio_sub_title = $gallery_page['gallery_portfolio_sub_title'];
			$gallery_vest = $gallery_page['gallery_vest'];
			$gallery_accessories = $gallery_page['gallery_accessories'];
			$gallery_shoes = $gallery_page['gallery_shoes'];
			$gallery_wedding = $gallery_page['gallery_wedding'];
			$gallery_interior_design = $gallery_page['gallery_interior_design'];
		?>

	    <!-- Masthead-->
	    <header class="masthead">
            <div class="container">
                <div class="masthead-subheading"><?php echo $gallery_header_title ; ?></div>
                <div class="masthead-heading"><?php echo $gallery_header_sub_title ; ?></div>
                <a class="btn btn-outline-primary btn-xl js-scroll-trigger" href="<?php echo home_url( '#about' ); ?>"><?php echo $gallery_header_button ; ?></a>
            </div>
        </header>

        <section class="page-section bg-light" id="">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase"><?php echo $gallery_portfolio_title ; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $gallery_portfolio_sub_title ; ?>.</h3>
                </div>
                <h4 class="subheading"><?php echo $gallery_vest ; ?></h4>
				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'veste',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$veste_query = new WP_Query( $args );

					if( $veste_query -> have_posts() ) : while( $veste_query -> have_posts() ) : $veste_query -> the_post(); ?>
				
					<div class="col-lg-3 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'veste-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>

				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'vesteGalileen',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$vesteGalileen_query = new WP_Query( $args );

					if( $vesteGalileen_query -> have_posts() ) : while( $vesteGalileen_query -> have_posts() ) : $vesteGalileen_query -> the_post(); ?>
				
					<div class="col-lg-6 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'vesteGalileen-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>

				<h4 class="subheading"><?php echo $gallery_accessories ; ?></h4>
				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'accessoire',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$accessoire_query = new WP_Query( $args );

					if( $accessoire_query -> have_posts() ) : while( $accessoire_query -> have_posts() ) : $accessoire_query -> the_post(); ?>
				
					<div class="col-lg-3 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'accessoire-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>

                <h4 class="subheading"><?php echo $gallery_shoes ; ?></h4>
				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'soulier',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$soulier_query = new WP_Query( $args );

					if( $soulier_query -> have_posts() ) : while( $soulier_query -> have_posts() ) : $soulier_query -> the_post(); ?>
				
					<div class="col-lg-3 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'soulier-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>

                <h4 class="subheading"><?php echo $gallery_wedding ; ?></h4>
				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'mariage',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$mariage_query = new WP_Query( $args );

					if( $mariage_query -> have_posts() ) : while( $mariage_query -> have_posts() ) : $mariage_query -> the_post(); ?>
				
					<div class="col-lg-3 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'mariage-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>
                <h4 class="subheading"><?php echo $gallery_interior_design ; ?></h4>
				<div class="row my-5">
					<?php
					$args = array(
					'post_type' => 'designInterieur',
					'post_status' => 'publish',
					'numberposts' => -1,
					'order' => 'DESC'
					);
					$designInterieur_query = new WP_Query( $args );

					if( $designInterieur_query -> have_posts() ) : while( $designInterieur_query -> have_posts() ) : $designInterieur_query -> the_post(); ?>
				
					<div class="col-lg-3 col-sm-6 mb-4 mb-lg-0">
						<div class="">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('large', array('class'=>'designInterieur-images img-fluid'));
								}
							?>
						</div>
					</div>
					<?php endwhile; endif; ?>
				</div>
            </div>
        </section>

	</main><!-- #main -->

<?php
get_footer();
